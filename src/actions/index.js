const booksRequested = () => {
	return {
		type: 'FETCH_BOOKS_REQUEST',
	}
};

const booksLoaded = (newBooks) => {
	return {
		type: 'FETCH_BOOKS_SUCCESS',
		payload: newBooks
	};
};

const booksError = (error) => {
	return {
		type: 'FETCH_BOOKS_FAILURE',
		payload: error,
	}
};

const fetchBooks = (bookstoreService) => () => (dispatch) => {
	dispatch(booksRequested());
	bookstoreService.getBooks()
		.then((data) => dispatch(booksLoaded(data)))
		.catch((error) => dispatch(booksError(error)))
};

const bookAddedToCart = (bookId) => {
	return {
		type: 'BOOK_ADDED_TO_CART',
		payload: bookId,
	}
};

const bookDecreaseInCart = (bookId) => {
	return {
		type: 'BOOK_DECREASE_FROM_CART',
		payload: bookId,
	}
};

const bookDeleteInCart = (bookId) => {
	return {
		type: 'BOOK_DELETE_FROM_CART',
		payload: bookId,
	}
};

export {
	fetchBooks,
	bookAddedToCart,
	bookDecreaseInCart,
	bookDeleteInCart,
};
