const calculateTotalPrice = (cartItems) => {
    return cartItems.reduce((total, item) => {
        return total + item.total;
    }, 0);
};

const updateCartItems = (book, cartItems) => {
    const isBookInCartItems = cartItems.find(item => book.id === item.id);
    if (isBookInCartItems) {
        const newCartItems = cartItems.reduce((total, item) => {
            if (item.id === book.id) {
                return [
                    ...total, {
                        ...item,
                        count: item.count + 1,
                        total: item.total + book.price,
                    }
                ]
            }
            return [...total, item];
        }, []);
        return {
            cartItems: newCartItems,
            orderTotal: calculateTotalPrice(newCartItems),
        }
    }
    const newCartItems = [
        ...cartItems,
        {
            id: book.id,
            title: book.title,
            count: 1,
            total: book.price,
        }
    ];
    return {
        cartItems: newCartItems,
        orderTotal: calculateTotalPrice(newCartItems),
    }
};

const bookDecreaseCartItems = (book, cartItems) => {
    const newCartItems = cartItems.reduce((total, item) => {
        if (book.id === item.id) {
            if (item.count === 1) {
                return total;
            }
            return [
                ...total,
                {
                    id: item.id,
                    title: item.title,
                    count: item.count - 1,
                    total: item.total - book.price,
                }
            ]
        }
        return [...total, item];
    }, []);
    return {
        cartItems: newCartItems,
        orderTotal: calculateTotalPrice(newCartItems),
    }
};

const bookDeleteCartItems = (book, cartItems) => {
    const newCartItems = cartItems.reduce((total, item) => {
        if (book.id === item.id) {
            return total;
        }
        return [...total, item];
    }, []);
    return {
        cartItems: newCartItems,
        orderTotal: calculateTotalPrice(newCartItems),
    }
};

const getBookById = (books, bookId) => {
    return books.find((book) => bookId === book.id);
};


const updateShoppingCart = (state, action) => {
    if (state === undefined) {
        return {
            cartItems: [],
            orderTotal: 0,
        }
    }
    switch (action.type) {
        case 'BOOK_ADDED_TO_CART':
            return updateCartItems(getBookById(state.bookList.books, action.payload), state.shoppingCart.cartItems);
        case 'BOOK_DECREASE_FROM_CART':
            return bookDecreaseCartItems(getBookById(state.bookList.books, action.payload), state.shoppingCart.cartItems);
        case 'BOOK_DELETE_FROM_CART':
            return bookDeleteCartItems(getBookById(state.bookList.books, action.payload), state.shoppingCart.cartItems);
        default:
            return state.shoppingCart;
    }
};

export default updateShoppingCart;
