import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import './shop-header.css';

const ShopHeader = ({numItems, total}) => {
	return (
		<header className="shop-header row">
			<Link to={'/'}>
				<div className="logo text-dark">ReStore</div>
			</Link>
				<div className="shopping-cart">
					<i className="cart-icon fa fa-shopping-cart"/>
					{numItems} items (${total})
				</div>
		</header>
	);
};

const mapStateToProps = ({shoppingCart: {orderTotal, cartItems}}) => {
	return {
		total: orderTotal,
		numItems: cartItems.length,
	}
};
export default connect(mapStateToProps)(ShopHeader);
