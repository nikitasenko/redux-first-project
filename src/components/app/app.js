import React from 'react';
import { Route, Switch } from 'react-router-dom';
import ShopHeader from '../shop-header';
import { HomePage } from '../pages';

import './app.css';

const App = () => {
  return (
    <main role="main" className="container">
      <ShopHeader/>
      <Switch>
        <Route
          path="/"
          component={HomePage}
          exact />
      </Switch>
    </main>
  );
};

export default App;
